<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php include 'includes/meta.php'; ?>
		<link rel="stylesheet" href="./view/style/news.css" type="text/css" />

        <title>Blog | ProgWeb</title>
    </head>

    <body>
		<?php
			// Vérification des données provenant du modèle
			if (isset($news))
			{
		?>
		<div id="particles-js"></div>

		<header>
			<?php include 'includes/header.php'; ?>
		</header>

		<main class="news">
			<img src="<?= $news->getImage()?>" class="img-fluid">
			<section>
				<h2><?= $news->getTitle()?></h2>
				<p class="dateNews"><?= $news->getDate()?></p>
				<p><?= nl2br($news->getContent())?></p>
			</section>

			<section id="comments">
				<h3>Commentaires</h3>

				<?php
					if ($comments != NULL)
					{
						foreach ($comments as $com)
						{
				?>

				<article>
					<p><strong><?= $com->getPseudo()?></strong> | <span class="commentDate"><?= $com->getDate()?></span></p>
					<hr/>
					<p><?= nl2br($com->getContent())?></p>
				</article>

				<?php
						}
					}
					else
					{
				?>

				<article>
					<p>Aucun commentaires pour le moment.</p>
				</article>

				<?php
					}
				?>

				<article class="add_comment_blog">
					<form method="post">
						<fieldset>
							<legend>Ajouter un commentaire</legend>
							<?php
								if (isset($dViewError) && count($dViewError)>0)
								{
							?>
							<div class="alert alert-danger" role="alert">
								<?php
									foreach ($dViewError as $value){
										echo $value.'<br/>';
									}
								?>
							</div>
							<?php
								}
							?>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<label class="input-group-text" for="pseudo">@</label>
								</div>
								<input name="pseudo" id="pseudo" type="text" class="form-control" placeholder="Pseudo" value="<?php if ($pseudo != '') echo $pseudo?>" required>
							</div>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">Votre commentaire</span>
								</div>
								<textarea name="comment" id="comment" class="form-control" aria-label="Votre commentaire" required><?php if ($pseudo == '') echo $comment?></textarea>
							</div>
							<input type="hidden" name="action" value="comment">
							<input type="hidden" name="newsID" value="<?= $news->getId()?>">
							<button type="submit" class="btn btn-primary mt-3">Envoyer</button>
						</fieldset>
					</form>
				</article>
			</section>
		</main>

        <footer class="mt-auto">
            <?php include 'includes/footer.php'; ?>
        </footer>

		<?php
			}
			else {
				require 'errors/error-405.php';
			}
		?>

        <script src="view/bootstrap/js/jquery.js"></script>
        <script src="view/bootstrap/js/bootstrap.js"></script>
		<script src="view/js/particles.js"></script>
		<script src="view/js/app.js"></script>
    </body>
</html>