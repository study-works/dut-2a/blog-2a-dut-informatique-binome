# Blog
Blog project of IUT

<p>A envoyer à : matthieu.restituito@uca.fr</p>

<div>
	<h2 style="color: blue"> Projets</h2>
	<div style="text-align: justify">
		<p style="font-weight: bold">Remarques sur les différents projets proposés.</p>
		<p>Le patron<span style="color:#0000FF"> <strong>MVC</strong> </span>doit etre utilisé. La gestion d'erreurs doit être complète. (champs vérifiés, connexion à la BD, etc.). Concernant l'interface, toutes les idées sont les bienvenues, la qualité de l'ergonomie de l'interface sera prise en compte.<br> Les plus téméraires peuvent aller plus loin (utilisation d'un framework simple ? Yii, ORM ?, templating avec Smarty/Twig ?)</p>
		<p>Le projet est à présenter et à rendre en semaine 7 (TP).</p>
		<h4><font color="#990000">Blog en MVC</font></h4>
		<p>Le but du projet est de proposer la gestion complète d'un blog (journal de "news" ajoutées par date) en PHP, en utilisant le patron <font color="#0000FF"><strong>MVC</strong></font>.<br>La page principale du blog est composée d'une première partie supérieure (ou d'un menu), permettant d'administrer son blog, et d'une seconde partie composée des news affichées de la sorte: date : news. Cette page principale doit être découpée en plusieurs pages appelées par des liens, si le blog est trop important.<br> <br>L'ensemble des news doivent être sauvegardées dans une base de données.<br>Une news peut être écrite par des administrateurs. Les utilisateurs normaux ne peuvent qu'ajouter des commentaires. Une news correspond à du texte écrit en HTML. La partie supérieure (ou le menu) est composée au moins:</p>
		<ul type="disc">
			<li>d'un lien ajout de commentaire: celui-ci fait appel à une nouvelle page demandant un pseudo et le message à ajouter dans le blog. Si le client ajoute plusieurs commentaires, le champ pseudo doit contenir le pseudo précédemment donné (utilisez une session)</li>
			<li>d'un lien administration: celui-ci fait appel à une page demandant un login et un mot de passe. La page suivante correspond soit à une page d'erreur (login, password erroné) soit à la page principale (c'est à dire l'ensemble des news par date) avec un bouton	supplémentaire "effacer" par news qui permet de supprimer la news du blog et d'un boutton ajouter news.</li>
			<li>d'un bouton rechercher qui permet de rechercher et d'afficher une news par date</li>
			<li>de 2 compteurs donnant le nombre de messages du blog (appel bd) et le nombre de messages du client actuellement connecté (via uncookie)</li>
			<li>optionnel : les champs tels que login, mot de passe, titre de news, peuvent aussi etre vérifiés en javascript (si vide, afficer une alerte). Un news peut être écrite en BBcode (bbcode) qui doit être traduit en HTML(ex: &lt;b&gt; &lt;/b&gt;doit afficher du texte en gras) et optionnellement des smileys</li>
		</ul>
		<p>La gestion d'erreurs doit être complète. (champs vérifiés, connection à la BD,...) </p>
	</div>
</div>
